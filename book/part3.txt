Podr�zny dlugo w oknie stal patrzac, dumajac,

Wonnymi powiewami kwiat�w oddychajac,

Oblicze az na krzaki fijolkowe sklonil,

Oczyma ciekawymi po drozynach gonil

I znowu je na drobnych sladach zatrzymywal,

Myslal o nich i, czyje byly, odgadywal.

Przypadkiem oczy podni�sl, i tuz na parkanie

Stala mloda dziewczyna. - Biale jej ubranie

Wysmukla postac tylko az do piersi kryje,

Odslaniajac ramiona i labedzia szyje.

W takim Litwinka tylko chodzic zwykla z rana,

W takim nigdy nie bywa od mezczyzn widziana:

Wiec choc swiadka nie miala, zalozyla rece

Na piersiach, przydawajac zaslony sukience.

Wlos w pukle nie rozwity, lecz w wezelki male

Pokrecony, schowany w drobne straczki biale,

Dziwnie ozdabial glowe, bo od slonca blasku

Swiecil sie, jak korona na swietych obrazku.

Twarzy nie bylo widac. Zwr�cona na pole

Szukala kogos okiem, daleko, na dole;

Ujrzala, zasmiala sie i klasnela w dlonie,

Jak bialy ptak zleciala z parkanu na blonie

I wionela ogrodem przez plotki, przez kwiaty,

I po desce opartej o sciane komnaty,

Nim spostrzegl sie, wleciala przez okno, swiecaca,

Nagla, cicha i lekka jak swiatlosc miesiaca.

N�cac chwycila suknie, biegla do zwierciadla;

Wtem ujrzala mlodzienca i z rak jej wypadla

Suknia, a twarz od strachu i dziwu pobladla.

Twarz podr�znego barwa splonela rumiana

Jak oblok, gdy z jutrzenka napotka sie ranna;

Skromny mlodzieniec oczy zmruzyl i przyslonil,

Chcial cos m�wic, przepraszac, tylko sie uklonil

I cofnal sie; dziewica krzyknela bolesnie,

Niewyraznie, jak dziecko przestraszone we snie;

Podr�zny zlakl sie, sp�jrzal, lecz juz jej nie bylo.

Wyszedl zmieszany i czul, ze serce mu bilo

Glosno, i sam nie wiedzial, czy go mialo smieszyc

To dziwaczne spotkanie, czy wstydzic, czy cieszyc.

