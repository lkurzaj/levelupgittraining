Przy mszy, gdy z wzniesionymi zwracal sie rekami

Od oltarza do ludu, by m�wic: "Pan z wami",

To nieraz tak sie zrecznie skrecil jednym razem,

Jakby "prawo w tyl" robil za wodza rozkazem,

I slowa liturgiji takim wyrzekl tonem

Do ludu, jak oficer stojac przed szwadronem;

Postrzegali to chlopcy sluzacy mu do mszy.

Spraw takze politycznych byl Robak swiadomszy

Nizli zywot�w swietych, a jezdzac po kwescie,

Czesto zastanawial sie w powiatowem miescie;

Mial pelno interes�w: to listy odbieral,

Kt�rych nigdy przy obcych ludziach nie otwieral,

To wysylal poslanc�w, ale gdzie i po co,

Nie powiadal; czestokroc wymykal sie noca

Do dwor�w panskich, z szlachta ustawicznie szeptal

I okoliczne wioski dokola wydeptal,

I w karczmach z wiesniakami rozprawial niemalo,

A zawsze o tem, co sie w cudzych krajach dzialo.

Teraz Sedziego, kt�ry juz spal od godziny,

Przychodzi budzic; pewnie ma jakies nowiny.