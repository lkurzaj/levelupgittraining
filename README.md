# README #

This README would normally document whatever steps are necessary to get your application up and running.
To work with this repo you need to have installed git application with git bash.

### What is this repository for? ###

* This repository is to learn git, to test git flow

### How do I get set up? ###

* write in console : git clone https://bitbucket.org/lkurzaj/levelupgittraining.git
* go to repo directory: cd levelupgittraining
* get newest remote repo version: write in console git fetch
* switch branch to exercise1: write in console git checkout exercise1
* switch to new branch exercise1_partX, where x means your number on names list: write in console git checkout -b exercise1_partX
* Create on levelupgittraining directory file named content.txt
* move content from book/partX.txt to content.txt save it and commit
* push committed changes to remote and create pull request on bitbucket. Pull request should be called to exercise1 branch
* resolve conflicts